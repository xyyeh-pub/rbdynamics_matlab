## rbdynamics_matlab - rigid body dynamics library for robot analysis

rbdynamics_matlab is a matlab library created for computing dynamics and kinematics of articulated rigid body systems. It is based upon Featherstone's spatial vectors library. Several useful 
functionalities are also included to allow user easy setup joint and task space dynamic parameters. As of now, only serial chains are supported.